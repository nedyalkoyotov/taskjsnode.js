require('./app/authentication/passportconfiguration')(passport);
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const restApi = require('./rest');
const api = require('express').Router();
const homepage = require('./public/index.html');
const task = require('./public/task.html');
const aboutus = require('./public/aboutus.html');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//Handle successful response
app.use((req, res, next) => {

    let json = res.json.bind(res);

    res.json = (data) => {

        let returnedData;
        let response = res;

        if (data instanceof Error) {
            returnedData = {"error": data.message, "status": false};
            if (data.status) {
                response.status(data.status);
            } else {
                response.status(500);
            }
        } else {
            returnedData = {'data': data, 'status': true};
        }
        return json(returnedData);
    };

    next();
});

//Handle errors
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).json({'data': err, 'status': false});
    next();
});

app.use('/homepage', express.static(homepage));
app.use('/task', express.static(task));
app.use('/aboutus', express.static(aboutus));

app.listen(port);
console.log('Application app started on: ' + port);

app.use(cookieParser()); // read cookies (needed for auth)
app.use(passport.initialize());

app.use(session({
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: true,
    store: new mongoStore({
        mongooseConnection: mongooseConnection
    })
}));
module.exports = app;